Clone repo
Open ServeTsp.kt
Click the green play icon
Open browser and point to: http://localhost:9000

When you have made a change to your algorithm, you need to click the green play icon again and refresh browser.

We are using chart.js which you can read up on here: https://www.chartjs.org/docs/latest/charts/scatter.html
