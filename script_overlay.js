<script>

var ctx = document.getElementById("<CHART_NAME>");

var myChart = new Chart(ctx, {

  type: 'scatter',
  data: {
     datasets: [{
        data: [<DATA>],
        label: "<LABEL_NAME>",
        borderColor: '#ff00d8',
        borderWidth: 2,
        pointBackgroundColor: ['#00ff12'],
        pointBorderColor: ['#00bcd6'],
        pointRadius: 5,
        pointHoverRadius: 50,
        fill: false,
        tension: 0,
        showLine: true
     },
     {
        data: [<DATA2>],
        label: "<LABEL_NAME2>",
        borderColor: '#476850',
        borderWidth: 2,
        pointBackgroundColor: ['#00ff12'],
        pointBorderColor: ['#00bcd6'],
        pointRadius: 5,
        pointHoverRadius: 50,
        fill: false,
        tension: 0,
        showLine: true
      }]
  },
  options: {
    interaction: {
     intersect: false,
     mode: 'index',
   },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true
        }
      }]
    },
  }
});


</script>