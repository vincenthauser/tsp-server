package se.bernhauser.tsp

import se.bernhauser.tsp.distance.Distance
import java.awt.Point
import kotlin.math.roundToInt

fun printAlgoResults(distance: Double, title: String) {
    println("Distance result for $title: ${distance.roundToInt()}")
}

//Use in loop
fun printImprovement(originalDistance: Double, oldDistance: Double, newDistance: Double, algo: String) {
    println("$algo found better route. Original distance: ${originalDistance.roundToInt()}, previous best: ${oldDistance.roundToInt()}, new distance: ${newDistance.roundToInt()}")
}

fun printFinalResults(distance: Double, improvedDistance: Double) {
    println("=============== FINAL RESULTS ===============")
    println("Old dist: ${distance.roundToInt()}, New dist: ${improvedDistance.roundToInt()}")
    println("\n")
    println("\n")
}

fun Solution.printLegs() {
    this.route.forEachIndexed { index, point ->
        print(point.hr() + " ")
        if (index + 1 < this.route.size) {
            val leg = Distance.getLegDistance(this.route[index], this.route[index + 1])
            print("d: $leg")
        }
        println()
    }
}

fun List<Point>.printList() {
    print("[ ")
    this.forEach { print("(${it.x},${it.y}), ") }
    print("]")
}

/**
 * Human Readable
 */
fun Point.hr(): String {
    return "{x: ${this.x}, y: ${this.y}}"
}