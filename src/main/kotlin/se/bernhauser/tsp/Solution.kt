package se.bernhauser.tsp

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import java.awt.Point

data class Solution(val route: List<Point>, val title: String, val distance: Double, val isOriginal: Boolean) {

    companion object {
        fun empty(): Solution {
            return Solution(listOf<Point>(), "empty", Double.MAX_VALUE, false)
        }

        fun fromJson(json: String): Solution {
            val jsonArray: JsonArray = Gson().fromJson(json, JsonArray::class.java)
            val json: JsonObject = jsonArray.get(0).asJsonObject

            val title = json.get("title").asString
            val distance = json.get("distance").asDouble
            val isOriginal = json.get("isOriginal").asBoolean
            val jsonRoute = json.getAsJsonArray("route")

            val route = ArrayList<Point>()
            jsonRoute.forEach { el ->
                val x = el.asJsonObject.get("x").asInt
                val y = el.asJsonObject.get("y").asInt
                route.add(Point(x, y))
            }

            return Solution(route, title, distance, isOriginal)
        }
    }

}
