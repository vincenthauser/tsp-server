package se.bernhauser.tsp

import se.bernhauser.tsp.fileutils.FileHandling.getTextFromFile

object StaticRoute {

    fun getRoute(): Solution {
        return Solution.fromJson(getStaticJson())
    }

    private fun getStaticJson(): String {
        return getTextFromFile("static_route.json")
    }
}
