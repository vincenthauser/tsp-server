package se.bernhauser.tsp

import com.google.gson.GsonBuilder
import se.bernhauser.tsp.algos.Algorithms
import se.bernhauser.tsp.randoms.RandomOperations
import se.bernhauser.tsp.validation.Validation
import java.lang.IllegalArgumentException
import se.bernhauser.tsp.fileutils.FileHandling.getTextFromFile
import se.bernhauser.tsp.fileutils.FileHandling.saveToFile

class Tsp {
    private val routesResult = arrayListOf<Solution>()

    fun runAllNewRoutes(): List<Solution> {

        //Generate all cities and a random route between each city.
        val seedRoute = RandomOperations.generateCitiesAndRandomRoute(30)

        //We use this as a comparison down the road.
        val originalDistance = seedRoute.distance

        //Add seed route for comparison purposes.
        routesResult.add(seedRoute)

        //Add all other algos' results that are implemented.
        val results = Algorithms.runAlgorithms(seedRoute.route, originalDistance)
        routesResult.addAll(results)

        //Validate before saving.
        if (!Validation.isValid(routesResult)) {
            throw IllegalArgumentException("Lists are not the same in content.")
        }

        saveLastRun(seedRoute)

        return routesResult
    }

    fun runSpecificRoute(solution: Solution): List<Solution> {
        val originalDistance = solution.distance
        routesResult.add(solution)

        val results = Algorithms.runAlgorithms(solution.route, originalDistance)
        routesResult.addAll(results)

        //Validate
        if (!Validation.isValid(routesResult)) {
            throw IllegalArgumentException("Lists are not the same in content")
        }

        return routesResult
    }

    /**
     * Copy the results from last_run.json into static_route.json for now
     */
    private fun saveLastRun(seedRoute: Solution) {
        val lastRun = ArrayList<Solution>(1)
        lastRun.add(seedRoute)
        saveLastRun(solutions = lastRun)
    }

    private fun saveLastRun(solutions: List<Solution>) {
        val gson = GsonBuilder().setPrettyPrinting().create()
        saveToFile("last_run.json", gson.toJson(solutions))
    }

    fun getLastRun(): List<Solution> {
        return runSpecificRoute(Solution.fromJson(getTextFromFile("last_run.json")))
    }

}
