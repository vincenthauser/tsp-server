package se.bernhauser.tsp

import se.bernhauser.tsp.formatters.OutputFormatter
import se.bernhauser.tsp.formatters.OutputType
import java.io.File
import java.util.*

/**
 * Run this from your console and print to output file
 */
class TspMain {
    companion object {
        private const val INVALID_PATH = "<your_path>"
        private const val PATH = INVALID_PATH // Set your path here

        @JvmStatic
        fun main(args: Array<String>) {
            val results = Tsp().runAllNewRoutes()
            val outputType = OutputType.JSON //CSV - open in Excel to display data for example
            val data = OutputFormatter.getFormattedOutput(results, outputType)
            val fileName = "output"
            val filePath = "$PATH/$fileName.${outputType.name.lowercase(Locale.getDefault())}"

            if (filePath.contains(INVALID_PATH)) {
                throw IllegalArgumentException("You need to set the correct path in TspMain")
            }

            val file = File(filePath)

            file.printWriter().use { fileOut ->
                fileOut.println(data)
            }
        }
    }
}
