package se.bernhauser.tsp.algos

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import java.awt.Point

object Algorithms {

    private const val RUN_NN = true
    private const val RUN_RANDOM = false
    private const val RUN_CROSS_CHECK = false
    private const val RUN_CLUSTER_FLIP = false
    private const val RUN_CLUSTER_CHECK = false
    private const val RUN_TWO_POINT_SHIFT = false
    private const val RUN_TWO_POINT_SHIFT_2 = false
    private const val RUN_INTERSECTION_CHECK = true
    private const val RUN_TWO_POINT_SHIFT_INC = false

    /**
     * Your solution per algorithm is saved as a Solution ("snapshot")
     *
     * Each algorithm's result can be added to the list snapshots.
     *
     * Add your own algos here and add as a snapshot. Or you might just want to save the best Solution.
     *
     * Check out Distance.kt for operations
     */
    fun runAlgorithms(originalRoute: List<Point>, originalDistance: Double): List<Solution> {
        var step = ""
        var counter = 0

        try {

            val snapshots = arrayListOf<Solution>()
            var route = originalRoute

            var bestList = originalRoute

            while (counter < 5) {

                println("Starting run no. $counter with route: ${route.computeTotalDistance()} ")

                if (RUN_RANDOM) {
                    step = "RUN_RANDOM"
                    route = runRandom("RandomRoute_$counter", route, originalDistance, snapshots)
                }

                if (RUN_NN && counter == 0) {
                    step = "RUN_NN"
                    route = runNearestNeighbor("NearestNeighbor_$counter", route, originalDistance, snapshots)
                }

                if (RUN_CLUSTER_FLIP) {
                    step = "RUN_CLUSTER_FLIP"
                    route = clusterFlip(8, "FlipNN_$counter", route, originalDistance, snapshots)
                }

                if (RUN_CLUSTER_CHECK) {
                    step = "RUN_CLUSTER_CHECK"
                    route = runClusterCheck(8, "ClusterFix_$counter", route, originalDistance, snapshots)
                }

                if (RUN_TWO_POINT_SHIFT) {
                    step = "RUN_TWO_POINT_SHIFT"
                    route = runShiftTwoPoints("TwoPointShift_$counter", route, originalDistance, snapshots)
                }

                if (RUN_CLUSTER_FLIP) {
                    step = "RUN_CLUSTER_FLIP"
                    route = clusterFlip(8, "Flip2Point_$counter", route, originalDistance, snapshots)
                }

                if (RUN_TWO_POINT_SHIFT_2) {
                    step = "RUN_TWO_POINT_SHIFT_2"
                    route = runShiftTwoPoints("TwoPointShift2_$counter", route, originalDistance, snapshots)
                }

                if (RUN_TWO_POINT_SHIFT_INC) {
                    step = "RUN_TWO_POINT_SHIFT_INC"
                    route = runShiftTwoPointsInc("TwoPointShiftInc_$counter", route, originalDistance, snapshots)
                }

                if (RUN_CLUSTER_FLIP) {
                    step = "RUN_CLUSTER_FLIP"
                    route = clusterFlip(8, "ClusterFlip_$counter", route, originalDistance, snapshots)
                }

                if (RUN_CLUSTER_CHECK) {
                    step = "RUN_CLUSTER_CHECK2"
                    route = runClusterCheck(27, "ClusterFix2_$counter", route, originalDistance, snapshots)
                }

                if (RUN_INTERSECTION_CHECK) {
                    step = "RUN_INTERSECTION_CHECK"
                    runIntersectionCheck("Intersection", route, originalDistance, snapshots)
                }

                //Add your own algos here...
                //snapshot.add(yourAlgo)

                counter++
            }

            if (RUN_CROSS_CHECK) {
                step = "RUN_CROSS_CHECK"
                route = runCrossCheck("CrossCheck_$counter", route, originalDistance, snapshots)
                if (route.computeTotalDistance() < bestList.computeTotalDistance()) {
                    //bestList = route
                    println("Improved distance CROSS_CHECK: $counter")
                    println("Improved distance CROSS_CHECK: route: ${route.computeTotalDistance()} best: ${bestList.computeTotalDistance()}")
                }
            }

            return snapshots

        } catch (e: Exception) {
            println("Error main - error: $e")
            println("In step $step")
        }
        return listOf()
    }

    private fun runIntersectionCheck(
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ) {

        val unCrossed = Intersects.sort(title, route, originalDistance, 0)

        println("FinishedRun $title has route: ${unCrossed.route.computeTotalDistance()}")
        println()
        if (unCrossed.route.isNotEmpty() && unCrossed.distance < originalDistance) {
            snapshots.add(unCrossed)
        }
    }

    private fun runShiftTwoPointsInc(
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        val distance = route.computeTotalDistance()

        val shifted = ShiftTwoPointsIncrementally.sort(title, route, originalDistance, -1)
        if (shifted.route.isNotEmpty() && shifted.distance < distance) {
            snapshots.add(shifted)
            println("Improved distance SHIFT_INC: ${shifted.distance} route: ${shifted.route.computeTotalDistance()}")
            return ArrayList(shifted.route)
        } else {
            return ArrayList(route)
        }
    }

    private fun runShiftTwoPoints(
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        val distance = route.computeTotalDistance()
        val shifted = ShiftTwoPoints.sort(title, route, originalDistance, route.size - 1)
        if (shifted.route.isNotEmpty() && shifted.distance < distance) {
            snapshots.add(shifted)
            println("Improved distance SHIFT: ${shifted.distance}")
            return ArrayList(shifted.route)
        }
        return ArrayList(route)
    }

    private fun runClusterCheck(
        peekDistance: Int,
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        //Try to randomly improve some clusters
        val distance = route.computeTotalDistance()
        val cluster = ClusterCheck(peekDistance, false)
        val clusterCheck = cluster.sort(title, route, originalDistance, 10)
        if (clusterCheck.route.isEmpty() && clusterCheck.distance < distance) {
            snapshots.add(clusterCheck)
            println("Improved distance CLUSTER_CHECK: ${clusterCheck.distance}")
            return ArrayList(clusterCheck.route)
        }
        return ArrayList(route)
    }

    private fun clusterFlip(
        peekDistance: Int,
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        println("clusterFlip() Flipping route..., original distance: $originalDistance")
        val distance = route.computeTotalDistance()
        val flipped = route.reversed()
        return runClusterCheck(peekDistance, title, flipped, distance, snapshots)
    }

    /**
     * Run nearest neighbor
     */
    private fun runNearestNeighbor(
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        val distance = route.computeTotalDistance()
        val nn = NearestNeighbor.sort(title, route, originalDistance, 10)
        if (nn.route.isNotEmpty() && nn.distance < distance) {
            snapshots.add(nn)
            println("Improved distance NN: ${nn.distance}")
            return ArrayList(nn.route)
        }
        return ArrayList(route)
    }

    private fun runRandom(
        title: String,
        route: List<Point>,
        originalDistance: Double,
        snapshots: ArrayList<Solution>
    ): ArrayList<Point> {
        //A naïve solution where we randomly generate a new route
        val randomImprovement = RandomSort.sort(title, route, originalDistance, 10000)
        //Add to snapshots so we can display later
        if (randomImprovement.route.isNotEmpty()) {
            snapshots.add(randomImprovement)
            return ArrayList(randomImprovement.route)
        }
        return ArrayList(route)
    }
}

private fun runCrossCheck(
    title: String,
    route: List<Point>,
    originalDistance: Double,
    snapshots: ArrayList<Solution>
): ArrayList<Point> {
    val distance = route.computeTotalDistance()
    val crossCheck = CrossCheck.sort(title, route, originalDistance, -1)
    if (crossCheck.route.isNotEmpty() && crossCheck.distance < distance) {
        snapshots.add(crossCheck)
        println("Improved distance CROSS_CHECK: ${crossCheck.distance}")
        return ArrayList(crossCheck.route)
    }
    return ArrayList(route)
}
