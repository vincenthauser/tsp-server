package se.bernhauser.tsp.algos

import java.awt.Point
import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.printAlgoResults

class ClusterCheck(private val peekDistance: Int, private val runWithIncreasingPeek: Boolean): Sort {

    companion object {
        private const val TITLE = "ClusterCheck"
    }

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        var bestDistance = Double.MAX_VALUE
        var bestSolution = Solution.empty()
        return if (runWithIncreasingPeek) {
            for (i in 2..peekDistance) {
                val newSolution = sortOnLegDistance(i, route, originalDistance = distance, numberOfRuns = numberOfRuns, theTitle)
                if (newSolution.distance < bestDistance) {
                    bestDistance = newSolution.distance
                    bestSolution = newSolution
                    println("$TITLE found new solution with $bestDistance with route: ${bestSolution.distance}")
                }
            }
            bestSolution
        } else {
            sortOnLegDistance(peekDistance, route, originalDistance = distance, numberOfRuns = numberOfRuns, theTitle)
        }
    }

    private fun sortOnLegDistance(peekDistance: Int, route: List<Point>, originalDistance: Double, numberOfRuns: Int, title: String): Solution {
        var bestList = route.toMutableList()
        var toSort = route.toMutableList()

        var index = 0
        var nextIndex = index + peekDistance
        var foundImprovement = false

        for (x in 1 until numberOfRuns) {
            for (i in 0 until toSort.size) {
                toSort = bestList
                while (nextIndex < toSort.size) {
                    val slice = toSort.subList(index, nextIndex)
                    val distance = slice.computeTotalDistance()
                    val clusterSort = RandomSort.sort(null, slice, distance, 100)
                    val newDistance = clusterSort.route.computeTotalDistance()
                    if (newDistance < distance && clusterSort.route.isNotEmpty()) {
                        for ((counter, c) in (index until nextIndex).withIndex()) {
                            toSort[c] = clusterSort.route[counter]
                        }
                    }
                    index += peekDistance
                    nextIndex += peekDistance
                }
                index = i
                nextIndex = index + peekDistance

                val hasImproved = Distance.hasImproved(bestList, toSort)
                if (hasImproved.hasImproved) {
                    foundImprovement = true
                    bestList = toSort
                }
            }
        }

        val bestDistance = bestList.computeTotalDistance()

        if (foundImprovement)  printAlgoResults(bestDistance, TITLE)

        return Solution(bestList, title, bestDistance, false)
    }
}
