package se.bernhauser.tsp.algos

import java.awt.Point
import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance
import se.bernhauser.tsp.hr
import se.bernhauser.tsp.utils.EdgeFinder
import se.bernhauser.tsp.utils.PointUtils

@Deprecated("use Intersects")
object CrossCheck : Sort {

    private const val TITLE = "CrossCheck"
    private val crossings = arrayListOf<Crossing>()

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        return crossCheck(route, originalDistance = distance, numberOfRuns = numberOfRuns, theTitle)
    }

    private fun crossCheck(route: List<Point>, originalDistance: Double, numberOfRuns: Int, title: String): Solution {
        var crossCount = 0

        for (start in route.indices) {

            if (start + 1 >= route.size) {
                break
            }

            val second = start + 1
            val p1 = route[start]
            val q1 = route[second]

            val third = second + 1

            for (i in third until route.size) {

                if (i + 1 >= route.size) {
                    break
                }

                val p2 = route[i]
                val q2 = route[i + 1]

                //print("Checking ${p1.hr()}, ${q1.hr()} :  ${p2.hr()}, ${q2.hr()}")

                if (PointUtils.twoPointsEqualTwoOthers(p1, q1, p2, q2)) {
                    println("")
                    continue
                }

                if (slopeIntercept(p1, q1, p2, q2)) {
                    crossCount++
                    crossings.add(Crossing(p1, q1, p2, q2))
                }
            }
        }

        println("FOUND ${crossings.size} crosses")

        if (crossCount == 0) {
            return Solution.empty()
        }

        try {
            val max = EdgeFinder.findMaxEdge(crossings)
            val np1 = Distance.getNearestPoint(max[0], max[1], route)
            val np2 = Distance.getNearestPoint(max[1], max[0], route)

            println("Nearest Max: ${np1.hr()}, ${np2.hr()}")
            val min = EdgeFinder.findMinEdge(crossings)
            val nnp1 = Distance.getNearestPoint(min[0], min[1], route)
            val nnp2 = Distance.getNearestPoint(min[1], min[0], route)
            println("Nearest Min: ${nnp1.hr()}, ${nnp2.hr()}")
        } catch (e: java.lang.Exception) {
            println("Error trying to find nearest point")
        }

        return Solution.empty()
    }

    // y = mx+b
    private fun slopeIntercept(p1: Point, p2: Point, p3: Point, p4: Point): Boolean {
        return doIntersect(p1, p2, p3, p4)
    }

    private fun orientation(p: Point, q: Point, r: Point): Int {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.

        val res = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)

        if (res == 0) { // collinear
            return 0
        }

        return if (res > 0) 1 else 2 // clock or counter-clock wise
    }

    private fun onSegment(p: Point, q: Point, r: Point): Boolean {
        return q.x <= p.x.coerceAtLeast(r.x) && q.x >= p.x.coerceAtMost(r.x) && q.y <= p.y.coerceAtLeast(r.y) && q.y >= p.y.coerceAtMost(r.y)
    }

    private fun doIntersect(p1: Point, q1: Point, p2: Point, q2: Point): Boolean {
        // Find the four orientations needed for general and
        // special cases
        val o1 = orientation(p1, q1, p2);
        val o2 = orientation(p1, q1, q2);
        val o3 = orientation(p2, q2, p1);
        val o4 = orientation(p2, q2, q1);

        // General case
        if (o1 != o2 && o3 != o4) {
            return true;
        }

        // Special Cases
        // p1, q1 and p2 are collinear and p2 lies on segment p1q1
        if (o1 == 0 && onSegment(p1, p2, q1)) return true;

        // p1, q1 and q2 are collinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(p1, q2, q1)) return true;

        // p2, q2 and p1 are collinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(p2, p1, q2)) return true;

        // p2, q2 and q1 are collinear and q1 lies on segment p2q2
        if (o4 == 0 && onSegment(p2, q1, q2)) return true;

        return false; // Doesn't fall in any of the above cases
    }

    /**
     * y	= m (x − Px) + Py
     *
     * Where:
     *
     * x,y	are the coordinates of any point on the line
     * m	is the slope of the line
     * Px , Py	x and y coordinates of the given point P that defines the line
     *
     * The slope (m) is the "steepness" of the line
     */
    private fun slope(p1: Point, p2: Point): Double {
        val r1 = (p2.y - p1.y).toDouble()
        val r2 = (p2.x - p1.x).toDouble()
        return r1 / r2
    }
}

data class Crossing(val p1: Point, val q1: Point, val p2: Point, val q2: Point)
