package se.bernhauser.tsp.algos

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.utils.PointUtils.short
import java.awt.Point

object Intersects : Sort {
    data class Intersection(val p1: Point, val p2: Point, val p3: Point, val p4: Point)

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        println("FIRST: ${route[0].short()}")
        println("SECOND: ${route[1].short()}")
        var sorted: Solution = Solution.empty()
        val intersections = intersections(route)
        intersections.forEach {
            val p1 = it.p1
            val p2 = it.p2
            val p3 = it.p3
            val p4 = it.p4
            println("INTERSECT:")
            println("for : p1 ${p1.short()} - p2: ${p2.short()}")
            println("for : p3 ${p3.short()} - p4: ${p4.short()}")
        }

        if (intersections.isNotEmpty()) {
            return unCross(intersections, route, distance)
        }
        return sorted
    }


    private fun unCross(intersections: List<Intersection>, route: List<Point>, distance: Double): Solution {
        val toKeep = ArrayList<Point>()
        var isOriginal = true
        val toSort = route.toMutableList()
        intersections.forEach {
            val pb: Int = pointBefore(it.p1, route)

            val subList = route.subList(pb, route.size)
            val subDistance = subList.computeTotalDistance()
            println("SECTION START: ${route[pb]}")
            println("SUBLIST dist: ${subList.computeTotalDistance()}")
            println("SECTION LAST: ${subList.last()}")

            //The Sub-solution here (RandomSort is just an example)
            val subSort: Solution = RandomSort.sort("NN Cross", subList, subDistance, 5)

            println("SUBLIST dist nn: ${subSort.route.computeTotalDistance()}")

            if (subSort.route.isNotEmpty() && subSort.distance < subDistance) {
                println("SUBLIST SHORTER!")
                toSort.addAll(route.subList(0, pb - 1))
                toSort.addAll(subSort.route)
                if (toSort.size != route.size) {
                    throw Exception("size not same")
                }
                val newDistance = toSort.computeTotalDistance()
                if (newDistance < distance) {
                    toKeep.clear()
                    toKeep.addAll(toSort)
                    isOriginal = false
                }
            }

        }

        return Solution(toKeep, "Uncroxx", toKeep.computeTotalDistance(), isOriginal)
    }

    private fun pointBefore(p: Point, route: List<Point>): Int {

        return route.indexOf(p) - 1
    }

    private fun intersections(route: List<Point>): List<Intersection> {

        val intersections = ArrayList<Intersection>()
        val len = route.size
        for (index in 1 until len - 2) {
            val p1 = route[index]
            val p2 = route[index + 1]
            val x1 = p1.x
            val x2 = p2.x

            val y1 = p1.y
            val y2 = p2.y

            // We won't catch intersections if both lines from a point intersects on each line
            for (nextIndex in (index + 2) until len - 2) {
                val p3 = route[nextIndex]
                val p4 = route[nextIndex + 1]

                if (doIntersect(p1, p2, p3, p4)) {
                    intersections.add(Intersection(p1, p2, p3, p4))
                }
            }
        }
        return intersections
    }

    // Given three collinear points p, q, r, the function checks if
    // point q lies on the line segment 'pr'
    private fun onSegment(p: Point, q: Point, r: Point): Boolean {
        return q.x <= kotlin.math.max(p.x, r.x) &&
                q.x >= kotlin.math.min(p.x, r.x) &&
                q.y <= kotlin.math.max(p.y, r.y) &&
                q.y >= Math.min(p.y, r.y)
    }

    // To find orientation of ordered triplet (p, q, r).
    // The function returns following values
    // 0 --> p, q and r are collinear
    // 1 --> Clockwise
    // 2 --> Counterclockwise
    private fun orientation(p: Point, q: Point, r: Point): Int {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        val results = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)
        if (results == 0) return 0 // collinear
        return if (results > 0) 1 else 2 // clock or counter clock wise
    }

    // The main function that returns true if line segment 'p1q1'
    // and 'p2q2' intersect.
    private fun doIntersect(p1: Point, q1: Point, p2: Point, q2: Point): Boolean {
        // Find the four orientations needed for general and
        // special cases
        val o1 = orientation(p1, q1, p2)
        val o2 = orientation(p1, q1, q2)
        val o3 = orientation(p2, q2, p1)
        val o4 = orientation(p2, q2, q1)

        // General case
        if (o1 != o2 && o3 != o4) return true

        // Special Cases
        // p1, q1 and p2 are collinear and p2 lies on segment p1q1
        if (o1 == 0 && onSegment(p1, p2, q1)) return true

        // p1, q1 and q2 are collinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(p1, q2, q1)) return true

        // p2, q2 and p1 are collinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(p2, p1, q2)) return true

        // p2, q2 and q1 are collinear and q1 lies on segment p2q2
        return if (o4 == 0 && onSegment(p2, q1, q2)) true else false
        // Doesn't fall in any of the above cases
    }
}
