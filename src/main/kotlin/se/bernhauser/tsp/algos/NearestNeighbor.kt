package se.bernhauser.tsp.algos

import java.awt.Point
import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.printAlgoResults
import se.bernhauser.tsp.printImprovement

object NearestNeighbor: Sort {

    private const val TITLE = "NearestNeighbor"

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        return sortOnLegDistance(route, originalDistance = distance, numberOfRuns = numberOfRuns, theTitle)
    }

    /**
     * Sort on nearest neighbor.
     *
     * Returns best result of the number of runs.
     *
     * Usually finds the best results within the first 4 iterations. After that improvements will be hard to find.
     */
    private fun sortOnLegDistance(route: List<Point>, originalDistance: Double, numberOfRuns: Int, title: String): Solution {
        val toSort = route.toMutableList()
        var lastDistance = originalDistance
        var sorted: Solution = Solution.empty()
        for (i in 1..numberOfRuns) {
            val legSort = sortOnLegDistance(title, toSort)
            val distance = legSort.route.computeTotalDistance()

            if (distance < lastDistance) {
                sorted = legSort
                printImprovement(originalDistance, lastDistance, distance, "sortOnLegDistance")
                lastDistance = distance
                toSort.clear()
                toSort.addAll(legSort.route)
            }
        }

        printAlgoResults(lastDistance, TITLE)

        return if (sorted.route.isEmpty()) {
            Solution(route, title, originalDistance, false)
        } else {
            sorted
        }
    }

    private fun sortOnLegDistance(title: String, list: List<Point>): Solution {
       return Distance.sortOnLegDistance(title, list)
    }
}
