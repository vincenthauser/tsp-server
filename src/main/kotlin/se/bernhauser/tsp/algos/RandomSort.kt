package se.bernhauser.tsp.algos

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.printAlgoResults
import se.bernhauser.tsp.randoms.RandomOperations
import java.awt.Point

object RandomSort : Sort {
    private const val TITLE = "RandomShuffle"

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        return shuffle(route, distance = distance, numberOfRuns = numberOfRuns, theTitle)
    }

    /**
     * Shuffle routes and return the one with the shortest distance.
     */
    private fun shuffle(route: List<Point>, distance: Double, numberOfRuns: Int, title: String): Solution {
        val toRandomize = route.toMutableList()
        var foundImprovement = false
        var oldDistance = distance
        var improvedDistance = Double.MAX_VALUE
        var counter = 0
        var improvedRoute = ArrayList<Point>(toRandomize.size)
        while (counter < numberOfRuns) {
            val newRoutes = RandomOperations.shuffleRoute(toRandomize)
            val newDistance = newRoutes.computeTotalDistance()
            if (newDistance < oldDistance) {
                foundImprovement = true
                oldDistance = newDistance
                improvedDistance = newDistance
                improvedRoute = newRoutes
            }
            counter++
        }

        if (foundImprovement) {
            return Solution(
                route = improvedRoute,
                title = title,
                distance = improvedDistance,
                isOriginal = false
            )
        } else {
            return Solution.empty()
        }
    }
}
