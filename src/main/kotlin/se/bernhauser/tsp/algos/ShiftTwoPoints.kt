package se.bernhauser.tsp.algos

import java.awt.Point
import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.printAlgoResults

object ShiftTwoPoints : Sort {

    private const val TITLE = "ShiftTwoPoints"

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        return shiftTwoPoints(route = route, numberOfRuns = numberOfRuns, title = theTitle)
    }

    private fun shiftTwoPoints(route: List<Point>, numberOfRuns: Int, title: String): Solution {
        val toShift = route.toMutableList()
        var bestDistance = toShift.computeTotalDistance()
        val results = ArrayList<Point>(route.size)
        var foundImprovement = false

        for (x in 1..numberOfRuns) {
            for (index in 1 until toShift.size - 2) {
                val nextIndex = index + 1

                val p1 = toShift[index]
                val p2 = toShift[nextIndex]

                toShift[index] = p2
                toShift[nextIndex] = p1

                val shiftedDistance = toShift.computeTotalDistance()

                if (shiftedDistance < bestDistance) {
                    bestDistance = shiftedDistance
                    results.clear()
                    results.addAll(toShift)
                    foundImprovement = true
                }
            }
        }

        if (foundImprovement) {
            printAlgoResults(bestDistance, TITLE)
            return Solution(
                route = results,
                title = title,
                distance = bestDistance,
                isOriginal = false
            )
        }
        return Solution.empty()
    }
}
