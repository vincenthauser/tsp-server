package se.bernhauser.tsp.algos

import java.awt.Point
import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import se.bernhauser.tsp.printAlgoResults

object ShiftTwoPointsIncrementally : Sort {
    private const val TITLE = "ShiftTwoPointsIncrementally"

    override fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution {
        val theTitle = title ?: TITLE
        return shiftTwoPointsInc(route, theTitle)
    }

    private fun shiftTwoPointsInc(route: List<Point>, title: String): Solution {
        val toShift = route.toMutableList()
        var bestDistance = toShift.computeTotalDistance()
        var foundImprovement = false
        val best = ArrayList<Point>(route.size)

        for (index in 1 until toShift.size - 2) {

            for (nextIndex in index until toShift.size - 2) {
                val p1 = toShift[index]
                val p2 = toShift[nextIndex]

                toShift[index] = p2
                toShift[nextIndex] = p1

                val shiftedDistance = toShift.computeTotalDistance()

                if (shiftedDistance < bestDistance) {
                    bestDistance = shiftedDistance
                    foundImprovement = true
                    best.clear()
                    best.addAll(toShift)
                }
            }
        }

        if (foundImprovement) {
            printAlgoResults(bestDistance, TITLE)
            return Solution(
                route = best,
                title = title,
                distance = bestDistance,
                isOriginal = false
            )
        }
        return Solution.empty()
    }
}
