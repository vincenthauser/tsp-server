package se.bernhauser.tsp.algos

import se.bernhauser.tsp.Solution
import java.awt.Point

interface Sort {
    fun sort(title: String?, route: List<Point>, distance: Double, numberOfRuns: Int): Solution
}
