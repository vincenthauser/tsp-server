package se.bernhauser.tsp.distance

import se.bernhauser.tsp.Solution
import java.awt.Point
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

object Distance {

    private const val SQUARED: Double = 2.0

    fun getBestSolution(solutions: List<Solution>): Solution {
        val shortestDistance = getBestDistance(solutions)
        solutions.forEach {
            if (it.distance.roundToInt() == shortestDistance) {
                return it
            }
        }
        return Solution.empty()
    }

    fun getBestDistance(solutions: List<Solution>): Int {
        var best = Double.MAX_VALUE
        solutions.forEach {
            if (it.distance < best) {
                best = it.distance
            }
        }
        return best.roundToInt()
    }

    fun getNearestPoint(p1: Point, p2: Point, route: List<Point>): Point {
        val shiftedRoute = route.toMutableList()
        val first = shiftedRoute.first()
        shiftedRoute.removeLast()
        val indexOfP1 = shiftedRoute.indexOf(p1)
        val temp: Point = shiftedRoute[indexOfP1]
        shiftedRoute[0] = temp
        shiftedRoute[indexOfP1] = first
        shiftedRoute.add(temp) //add as last also

        val nn = sortOnLegDistance("nada", shiftedRoute)
        nn.route.forEachIndexed { index, p ->
            if (p1 == p && (index + 1) < nn.route.size && (nn.route[index + 1] != p2)) {
                return nn.route[index + 1]
            }
        }
        return Point(-1, -1)
    }

    /**
     * Does new Solution have a shorter route?
     * Returns DistanceResults
     */
    fun hasImproved(oldRoute: List<Point>, newRoute: List<Point>): DistanceResults {
        if (newRoute.isEmpty()) {
            return DistanceResults.noResults()
        }

        val newDistance = newRoute.computeTotalDistance()
        val oldDistance = oldRoute.computeTotalDistance()
        val hasImproved = newDistance < oldDistance
        return DistanceResults(newDistance, oldDistance, hasImproved)
    }

    /**
     * Returns best distance value (smallest) of all snapshots
     */
    fun List<Solution>.getBestResults(): Double {
        var bestResults = Double.MAX_VALUE
        this.forEach { route ->
            if (route.distance < bestResults) {
                bestResults = route.distance
            }
        }
        return bestResults
    }

    /**
     * Total distance for this list of Points
     */
    fun List<Point>.computeTotalDistance(): Double {
        var distance: Double = 0.0
        val len = this.size
        this.forEachIndexed { index, point ->
            if (index + 1 < len) {
                val leg = getLegDistance(point, this[index + 1])
                distance += leg
            }
        }
        return distance
    }

    fun getLegDistance(first: Point, second: Point): Double {
        val theXs: Double = (second.x.toDouble() - first.x.toDouble()).pow(SQUARED)
        val theYs: Double = (second.y.toDouble() - first.y.toDouble()).pow(SQUARED)
        return sqrt(theXs + theYs)
    }

    fun sortOnLegDistance(title: String, list: List<Point>): Solution {
        val improvedRoute = ArrayList<Point>(list.size)
        improvedRoute.addAll(list)
        val lastPoint = improvedRoute.removeLast()

        for (i in 0 until improvedRoute.size - 1) {
            var indexWithHighestValue = 1
            val nextIndex = i + 1
            var nextLeg = Distance.getLegDistance(improvedRoute[i], improvedRoute[nextIndex])

            var foundImprovedLeg = false
            for (p in nextIndex until improvedRoute.size) {
                val nextNextLeg = Distance.getLegDistance(improvedRoute[i], improvedRoute[p])
                //println("Leg: ${nextLeg.roundToInt()} -> NextLeg: ${nextNextLeg.roundToInt()}")
                if (nextNextLeg < nextLeg) {
                    nextLeg = nextNextLeg
                    indexWithHighestValue = p
                    foundImprovedLeg = true
                }
            }

            if (foundImprovedLeg) {
                val temp = improvedRoute[i + 1]
                improvedRoute[i + 1] = improvedRoute[indexWithHighestValue]
                improvedRoute[indexWithHighestValue] = temp
            }
        }

        improvedRoute.add(lastPoint)

        return Solution(
            route = improvedRoute,
            title = title,
            distance = improvedRoute.computeTotalDistance(),
            isOriginal = false
        )
    }
}
