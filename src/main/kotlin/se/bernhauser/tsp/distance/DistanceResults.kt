package se.bernhauser.tsp.distance

data class DistanceResults(val newDistance: Double, val oldDistance: Double, val hasImproved: Boolean) {

    companion object {
        fun noResults() = DistanceResults(0.0, 0.0, false)
    }
}
