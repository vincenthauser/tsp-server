package se.bernhauser.tsp.fileutils

import java.io.File

object FileHandling {

    fun getTextFromFile(fileName: String): String {
        val output = StringBuffer()
        try {
            File(fileName).bufferedReader().readLines().forEach { out ->
                output.append(out).append("\n")
            }
        } catch (e: Exception) {
            println("ERROR. $e")
        }
        return output.toString()
    }

    fun saveToFile(fileName: String, data: String) {
        val file = File(fileName)
        file.printWriter().use { fileOut ->
            fileOut.println(data)
        }
    }
}
