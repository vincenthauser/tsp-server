package se.bernhauser.tsp.formatters

import se.bernhauser.tsp.Solution

class CsvFormatter : Formatter {

    override fun generateFormattedOutput(snapshots: List<Solution>, title: String): String {
        val out = StringBuffer()
        val originalRoute = snapshots.first { it.isOriginal } //get original first
        val improvedRoutes = snapshots.filter { !it.isOriginal } //get all others

        //print original first
        out.append("x;y;${originalRoute.title}\n")
        originalRoute.route.forEach { point ->
            out.append("${point.x};${point.y}\n")
        }

        out.append("Distance: ${originalRoute.distance}\n")
        out.append(";;\n")

        //print all others
        improvedRoutes.forEach { route ->
            out.append("x;y;${route.title}\n")
            route.route.forEach { point ->
                out.append("${point.x};${point.y}\n")
            }
            out.append("Distance: d: ${route.distance}\n")

            out.append("\n")
        }
        return out.toString()
    }
}
