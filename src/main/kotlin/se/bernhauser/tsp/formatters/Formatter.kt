package se.bernhauser.tsp.formatters

import se.bernhauser.tsp.Solution

interface Formatter {
    fun generateFormattedOutput(snapshots: List<Solution>, title: String): String
}
