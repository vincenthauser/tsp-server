package se.bernhauser.tsp.formatters

object FormatterFactory {
    fun getFormatter(type: OutputType): Formatter {
        return when (type) {
            OutputType.CSV -> CsvFormatter()
            OutputType.JSON -> JsonFormatter()
            OutputType.HTML -> HtmlFormatter()
        }
    }
}
