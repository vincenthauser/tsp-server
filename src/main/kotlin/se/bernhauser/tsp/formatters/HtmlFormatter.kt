package se.bernhauser.tsp.formatters

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance
import se.bernhauser.tsp.hr
import java.awt.Point
import java.io.File
import kotlin.math.roundToInt

class HtmlFormatter : Formatter {

    override fun generateFormattedOutput(snapshots: List<Solution>, title: String): String {
        if (snapshots.isEmpty()) {
            return getEmptyResultsDocument(styles = getStyles())
        }

        val divs = StringBuffer()
        val scripts = StringBuffer()
        var originalDistance = 1.0

        snapshots.firstOrNull { it.isOriginal }?.let {
            originalDistance = it.distance
        }

        generateBestResultsTop(snapshots, scripts, divs, originalDistance)

        snapshots.filter { it.route.isNotEmpty() }.forEach { solution ->
            scripts.append(generateScript(solution.title, solution))
            divs.append(generateDiv(solution.title, solution.distance, originalDistance))
        }

        //Overlay graph of last two runs that have different distances
        generateOverlayGraph(snapshots, scripts, divs, originalDistance)

        return getDocument(styles = getStyles(), divs = divs.toString(), scripts = scripts.toString(), results = generateResultsSummary(snapshots))
    }

    private fun generateOverlayGraph(snapshots: List<Solution>, scripts: StringBuffer, divs: StringBuffer, originalDistance: Double) {
        if (snapshots.size >= 2) {
            val list = snapshots.filter { !it.isOriginal }
            val lastDistance = list.last().distance.roundToInt()
            val solution1 = list.last()
            var solution2 = Solution.empty()
            for (i in list.size - 1 downTo 0) {
                val distance = list[i].distance.roundToInt()
                if (distance != lastDistance) {
                    solution2 = list[i]
                    break
                }
            }
            scripts.append(generateTwoLastAsOverlayScript(solution1, solution2))
            divs.append(generateDiv("CombinedChart", solution1.distance, originalDistance))
        }
    }

    private fun generateBestResultsTop(snapshots: List<Solution>, scripts: StringBuffer, divs: StringBuffer, originalDistance: Double) {
        val bestSolution = Distance.getBestSolution(snapshots)
        val templateName = "Best_${bestSolution.title}"
        val bestScript = generateScript(templateName, bestSolution)
        scripts.append(bestScript)
        divs.append(generateDiv(templateName, bestSolution.distance, originalDistance))
    }

    private fun generateResultsSummary(snapshots: List<Solution>): String {
        val results = StringBuffer()
        results.append("<h2>").append(Distance.getBestDistance(snapshots)).append("</h2>")
        results.append("<table>")
        results.append("<tr><th>Distance</th><th>Method</th></tr>")

        snapshots.forEach {
            results.append("<tr>")
            results.append("  <td>").append(it.distance.roundToInt().toString()).append("</td>")
            results.append("  <td>").append(it.title).append("</td>")
            results.append("</tr>")
        }
        results.append("</table>")
        return results.toString()
    }

    private fun generateDiv(title: String, distance: Double, originalDistance: Double): String {
        val div = getChartWrapperDiv()
        val decrease = originalDistance - distance
        val decreasePercentage = decrease / originalDistance * 100
        var outputDivs = div.replace("<DIV_NAME>", "wrapper_${title}")
        outputDivs = outputDivs.replace("<TITLE>", title)
        outputDivs = outputDivs.replace("<DISTANCE>", distance.roundToInt().toString())
        outputDivs = outputDivs.replace("<ORIGINAL_DISTANCE>", originalDistance.roundToInt().toString())
        outputDivs = outputDivs.replace("<PERCENTAGE>", decreasePercentage.roundToInt().toString())
        outputDivs = outputDivs.replace("<CHART_NAME>", title)

        return outputDivs
    }

    private fun generateScript(title: String, solution: Solution): String {
        val script = getScript()
        val dataPointsBuffer = StringBuffer()
        solution.route.forEach { point ->
            dataPointsBuffer.append(point.hr()).append(",")
        }

        val dataPoints = dataPointsBuffer.toString()
        val lastIndex = dataPoints.indexOfLast { it == ',' }

        var outputScripts = script.replace("<DATA>", dataPoints.substring(0, lastIndex))
        outputScripts = outputScripts.replace("<CHART_NAME>", title)

        return outputScripts
    }

    private fun generateTwoLastAsOverlayScript(solution1: Solution, solution2: Solution): String {
        if (solution1.route.isEmpty() || solution2.route.isEmpty()) {
            return "<p>Not enough data to generate overlays</p>"
        }

        val script = getOverlayScript()
        val dataPointsBuffer1 = StringBuffer()
        val dataPointsBuffer2 = StringBuffer()

        solution1.route.forEach { point ->
            dataPointsBuffer1.append(point.hr()).append(",")
        }

        solution2.route.forEach { point ->
            val newPoint = Point(point.x + 1, point.y + 1) //shift graph output a bit
            dataPointsBuffer2.append(newPoint.hr()).append(",")
        }
        val dataPoints1 = dataPointsBuffer1.toString()
        val dataPoints2 = dataPointsBuffer2.toString()
        val lastIndex1 = dataPoints1.indexOfLast { it == ',' }
        val lastIndex2 = dataPoints2.indexOfLast { it == ',' }

        var outputScripts = script.replace("<DATA>", dataPoints1.substring(0, lastIndex1))
        outputScripts = outputScripts.replace("<CHART_NAME>", "CombinedChart")
        outputScripts = outputScripts.replace("<LABEL_NAME>", solution1.title + ": " + solution1.distance.roundToInt().toString())
        outputScripts = outputScripts.replace("<DATA2>", dataPoints2.substring(0, lastIndex2))
        outputScripts = outputScripts.replace("<LABEL_NAME2>", solution2.title + ": " + solution2.distance.roundToInt().toString())

        return outputScripts
    }

    private fun getChartWrapperDiv(): String {
        return "\n<div class=\"<DIV_NAME>\"  style=\"width: $WRAPPER_WIDTH;\">\n" +
                "    <h1><TITLE></h1>\n" +
                "    <h2>Original Distance: <ORIGINAL_DISTANCE><br/>Distance: <DISTANCE><br/>Percentage Improved Compared to Original: <PERCENTAGE>%</h2>\n" +
                "    <canvas id=\"<CHART_NAME>\" width=\"$CHART_WIDTH\" height=\"$CHART_HEIGHT\"></canvas>\n" +
                "</div>\n"
    }

    private fun getTextFromFile(fileName: String): String {
        val output = StringBuffer()
        try {
            File(fileName).bufferedReader().readLines().forEach { out ->
                output.append(out).append("\n")
            }
        } catch (e: Exception) {
            println("ERROR. $e")
        }
        return output.toString()
    }

    private fun getStyles(): String {
        return getTextFromFile("styles.css")
    }

    private fun getScript(): String {
        return getTextFromFile("script.js")
    }

    private fun getOverlayScript(): String {
        return getTextFromFile("script_overlay.js")
    }

    private fun getEmptyResultsDocument(styles: String): String {
        return getTextFromFile("results.html")
            .replace("<TSP_STYLES>", styles)
            .replace("<TSP_CONTAINERS>", "No improvement")
            .replace("<TSP_SCRIPTS>", "")
            .replace("<TSP_RESULTS_WRAPPER>", "")
    }

    private fun getDocument(styles: String, divs: String, scripts: String, results: String): String {
        return getTextFromFile("results.html")
            .replace("<TSP_STYLES>", styles)
            .replace("<TSP_CONTAINERS>", divs)
            .replace("<TSP_SCRIPTS>", scripts)
            .replace("<TSP_RESULTS_WRAPPER>", results)
    }

    companion object {
        private const val CHART_WIDTH = 1000
        private const val CHART_HEIGHT = 800
        private const val WRAPPER_WIDTH = "70%"
    }
}
