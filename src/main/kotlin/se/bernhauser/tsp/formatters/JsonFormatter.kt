package se.bernhauser.tsp.formatters

import se.bernhauser.tsp.Solution
import com.google.gson.GsonBuilder

class JsonFormatter: Formatter {

    override fun generateFormattedOutput(snapshots: List<Solution>, title: String): String {
        val gson = GsonBuilder().setPrettyPrinting().create()
        return gson.toJson(snapshots)
    }

}
