package se.bernhauser.tsp.formatters

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.distance.Distance.getBestResults
import se.bernhauser.tsp.printFinalResults

object OutputFormatter {
    fun getFormattedOutput(solutions: List<Solution>, outputType: OutputType): String {
        val outputTitle = "Final output"
        //You want another type? Check out OutputFormatFactory.OutputType, or implement your own type.
        val formatter = FormatterFactory.getFormatter(outputType)
        val formattedOutput = formatter.generateFormattedOutput(solutions, outputTitle)
        val bestDistance = solutions.getBestResults()

        var originalDistance = 0.0
        solutions.firstOrNull() { it.isOriginal }?.let {
            originalDistance = it.distance
        }
        printFinalResults(originalDistance, bestDistance)
        return formattedOutput
    }
}
