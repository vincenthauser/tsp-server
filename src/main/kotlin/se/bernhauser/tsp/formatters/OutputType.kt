package se.bernhauser.tsp.formatters

enum class OutputType {
    CSV, //Export to .csv and you can open in Excel
    JSON,
    HTML //Returns a complete and runnable html page with charts using chart.js for displaying runs
}
