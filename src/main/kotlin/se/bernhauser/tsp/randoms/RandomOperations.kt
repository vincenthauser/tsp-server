package se.bernhauser.tsp.randoms

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.Tsp
import se.bernhauser.tsp.distance.Distance.computeTotalDistance
import java.awt.Point
import kotlin.random.Random

object RandomOperations {
    private const val MAX_X = 98
    private const val MAX_Y = 98

    fun shuffleRoute(route: List<Point>): ArrayList<Point> {
        val shuffledRoute = ArrayList<Point>()
        shuffledRoute.addAll(route)

        for (i in (shuffledRoute.size - 1) downTo 1) {
            if (i == shuffledRoute.size - 1) {
                continue
            }
            val randomIndex: Int = Random.nextInt(1, i + 1)
            val temp: Point = shuffledRoute[i]
            shuffledRoute[i] = shuffledRoute[randomIndex]
            shuffledRoute[randomIndex] = temp
        }
        return shuffledRoute
    }

    fun generateCitiesAndRandomRoute(numberOfCities: Int): Solution {
        val seedRoute = RandomOperations.generateRandomCities(numberOfCities)
        val originalDistance = seedRoute.computeTotalDistance()
        return Solution(seedRoute, "Original", originalDistance, true)
    }

    private fun generateRandomCities(numCities: Int): ArrayList<Point> {
        val list = arrayListOf<Point>()
        for (i in 1..numCities) {
            var point = getRandomPoint()
            while (list.contains(point)) {
                println("Ops, duplicate city: $point")
                point = getRandomPoint()
            }
            list.add(point)
        }
        list.add(list[0]) //Add starting point as last point
        return list
    }
    
    private fun getRandomPoint(): Point {
        val x = Random.nextInt(MAX_X)
        val y = Random.nextInt(MAX_Y)
        return Point(x, y)
    }
}
