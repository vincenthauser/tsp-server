package se.bernhauser.tsp.utils

import se.bernhauser.tsp.algos.Crossing
import se.bernhauser.tsp.hr
import java.awt.Point

object EdgeFinder {

    fun findMaxEdge(crossings: List<Crossing>): List<Point> {
        var x = 0
        var y = 0
        var pointX = Point()
        var pointY = Point()

        crossings.forEach {
            if (it.p1.x > x) {
                x = it.p1.x
                pointX = it.p1
            }

            if (it.p1.y > y) {
                y = it.p1.y
                pointY = it.p1
            }

            if (it.q1.x > x) {
                x = it.q1.x
                pointX = it.q1
            }

            if (it.q1.y > y) {
                y = it.q1.y
                pointY = it.q1
            }


            if (it.p2.x > x) {
                x = it.p2.x
                pointX = it.p2
            }

            if (it.p2.y > y) {
                y = it.p2.y
                pointY = it.p2
            }

            if (it.q2.x > x) {
                x = it.q2.x
                pointX = it.q2
            }

            if (it.q2.y > y) {
                y = it.q2.y
                pointY = it.q2
            }
        }
        println("Max Y and Max X  edges: ${pointX.hr()} and ${pointY.hr()}")
        val ret = ArrayList<Point>()
        ret.add(pointX)
        ret.add(pointY)
        return ret
    }

    fun findMinEdge(crossings: List<Crossing>): List<Point> {
        var x = Integer.MAX_VALUE
        var y = x
        var pointX = Point()
        var pointY = Point()

        crossings.forEach {
            if (it.p1.x < x) {
                x = it.p1.x
                pointX = it.p1
            }

            if (it.p1.y < y) {
                y = it.p1.y
                pointY = it.p1
            }

            if (it.q1.x < x) {
                x = it.q1.x
                pointX = it.q1
            }

            if (it.q1.y < y) {
                y = it.q1.y
                pointY = it.q1
            }


            if (it.p2.x < x) {
                x = it.p2.x
                pointX = it.p2
            }

            if (it.p2.y < y) {
                y = it.p2.y
                pointY = it.p2
            }

            if (it.q2.x < x) {
                x = it.q2.x
                pointX = it.q2
            }

            if (it.q2.y < y) {
                y = it.q2.y
                pointY = it.q2
            }
        }
        println("Min Y and Min X  edges: ${pointX.hr()} and ${pointY.hr()}")
        val ret = ArrayList<Point>()
        ret.add(pointX)
        ret.add(pointY)
        return ret
    }
}
