package se.bernhauser.tsp.utils

import java.awt.Point

object PointUtils {
    fun twoPointsEqualTwoOthers(p1: Point, p2: Point, p3: Point, p4: Point): Boolean {
        if (p1 == p3 || p1 == p4) {
            return true
        }
        if (p2 == p3 || p2 == p4) {
            return true
        }
        return false
    }

    fun Point.short(): String {
        return "[x=$x,y=$y]"
    }
}
