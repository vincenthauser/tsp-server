package se.bernhauser.tsp.validation

import se.bernhauser.tsp.Solution

object Validation {

    /**
     * Checks if all Points exists in all solutions AND checks if first and last are equal.
     * Checks if list sizes match
     */
    fun isValid(routes: List<Solution>): Boolean {
        val originalRoute = routes.first { it.isOriginal }
        val otherRoutes = routes.filter { !it.isOriginal }
        otherRoutes.forEach {
            if (originalRoute.route.size != it.route.size) {
                return false
            }
        }
        originalRoute.route.forEach { originalPoint ->
            otherRoutes.firstOrNull { it.route.contains(originalPoint) } ?: return false
        }
        return isFirstLastEqual(routes)
    }

    private fun isFirstLastEqual(routes: List<Solution>): Boolean =
        routes.firstOrNull { it.route.first() != it.route.last() } == null
}
