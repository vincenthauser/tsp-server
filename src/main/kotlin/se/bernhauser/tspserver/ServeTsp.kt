package se.bernhauser.tspserver

import io.javalin.Javalin
import io.javalin.http.Context
import se.bernhauser.tsp.StaticRoute
import se.bernhauser.tsp.Tsp
import se.bernhauser.tspserver.pagegeneration.PageGeneration
import java.lang.IllegalArgumentException

fun main() {
    val app = Javalin.create().start(9000)
    try {
        app.get("/") { ctx ->
            performNewRun(ctx)
        }

        // Use static route for improving specific parts of your algo
        app.get("/static") { ctx ->
            val results = Tsp().runSpecificRoute(StaticRoute.getRoute())
            ctx.html(PageGeneration.htmlResponse(results))
        }

        //Generate new route for each call to server
        app.get("/refresh") { ctx ->
            performNewRun(ctx)
        }

        app.get("/rerun") { ctx ->
            val results = Tsp().getLastRun()
            ctx.html(PageGeneration.htmlResponse(results))
        }

    } catch (e: IllegalArgumentException) {
        app.get("/") { ctx -> ctx.html("<h1>Error in your list items...</h1>") }
    }
}

private fun performNewRun(ctx: Context) {
    ctx.html(PageGeneration.htmlResponse(Tsp().runAllNewRoutes()))
}


