package se.bernhauser.tspserver.pagegeneration

import se.bernhauser.tsp.Solution
import se.bernhauser.tsp.formatters.OutputFormatter
import se.bernhauser.tsp.formatters.OutputType

object PageGeneration {
    fun htmlResponse(solutions: List<Solution>): String {
        return OutputFormatter.getFormattedOutput(solutions, OutputType.HTML)
    }
}
